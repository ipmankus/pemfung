
---Part 1---

-- 1
maLength::(Num b)=>[a]->b
maLength xs = sum (map (\x -> 1) xs) 

-- 2
--- it's equivalent of composing 2 functions together

-- 3
maIter::(Num a, Eq a)=>a->(b->b)->b->b
maIter n f x
    | (n == 0) = x
    | otherwise = maIter (n-1) f (f x) 

-- 4
--- ::(Num a, Eq a, Enum b)=>a->b->b
--- the effect is applying succ n times to an Enum, in case of Int then adding n.

-- 5
suSquare::(Num a, Enum a)=>a->a
suSquare n = foldr (+) 0 (map (^2) [1..n])

-- 6
--- mystery functions return the same array but reversed

-- 7
--- (id . f) x returns f(x), (f . id) x returns f(x), (f id) x yields error because of left associativity 

-- 8
--- TODO

-- 9
maFlip::(a->b->c)->(b->a->c)
maFlip f y x= g x y


---Part 2---
-- 1 
--- map (+1) xs

-- 2
--- TODO

-- 3
--- map (+2) (filter (>3) xs)

-- 4
--- map (\(x,y)->x+3) xs

-- 5
--- TODO


---Part 3---
--- TODO

-- PS: this document is poorly structured, and also some of the problems are hard to understand
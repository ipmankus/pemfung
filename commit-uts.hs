data Expr = C Float 
          | Expr :+ Expr 
          | Expr :- Expr 
          | Expr :* Expr 
          | Expr :/ Expr
          | V String 
          | Let String Expr Expr
  deriving Show

-- bonus 1
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v0 e0 (subst v1 e1 e2) 

-- bonus 2
turunan (C v) = C 0
turunan (V x) = C 1
turunan (e1 :+ e2) = (turunan e1) :+ (turunan e2)
turunan (e1 :- e2) = (turunan e1) :- (turunan e2)
turunan (e1 :* e2) = ((e1) :* (turunan e2)) :+ ((e2) :* (turunan e1)) 
turunan (e1 :/ e2) = (((e1) :* (turunan e2)) :- ((e2) :* (turunan e1))) :/ (e2 :* e2)
turunan (Let x e1 e2) = turunan (subst x e1 e2)

pythagoras = [(x, y, z) | z <- [5..], y <- [1..z], x <- [1..y], x*x + y*y == z*z]

fibs = 1:1:zipWith (+) (fibs) (tail fibs)

flipKu f = g
    where g y x = f x y

funLambda = \n -> \a -> \b -> n * (a-b)

-- turnBack = updateState (\s -> s {facing = right (right (facing s))})

fpb x y 
    | y == 0 = x
    | otherwise = fpb y (x `mod` y)
kpk x y = (x * y) `div` (fpb x y)   




data Expr = C Float 
          | Expr :+ Expr 
          | Expr :- Expr 
          | Expr :* Expr 
          | Expr :/ Expr
          | V String 
          | Let String Expr Expr
          | Pangkat Expr Expr
  deriving Show


subst v0 e0 (Pangkat e1 e2) = Pangkat (subst v0 e0 e1) (subst v0 e0 e2)

evaluate (Pangkat e1 e2) = evaluate(e1) ** evaluate(e2)
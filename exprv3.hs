data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :/ Expr | Expr :* Expr | V String | Let String Expr Expr 

substitute v0 e0 (V v1) = if (v0==v1) then e0 else (V v1)
substitute v0 e0 (C x) = (C x)
substitute v0 e0 (x :+ y) = (substitute v0 e0 x) :+ (substitute v0 e0 y)
substitute v0 e0 (x :- y) = (substitute v0 e0 x) :- (substitute v0 e0 y)
substitute v0 e0 (x :/ y) = (substitute v0 e0 x) :/ (substitute v0 e0 y)
substitute v0 e0 (x :* y) = (substitute v0 e0 x) :* (substitute v0 e0 y)
substitute v0 e0 (Let v x y) = Let v0 e0 (substitute v x y)

fold f1 f2 f3 f4 f5 f6 f7 (C x) = f1 x
fold f1 f2 f3 f4 f5 f6 f7 (x :+ y) = f2 (fold f1 f2 f3 f4 f5 f6 f7 x) (fold f1 f2 f3 f4 f5 f6 f7 y) 
fold f1 f2 f3 f4 f5 f6 f7 (x :- y) = f3 (fold f1 f2 f3 f4 f5 f6 f7 x) (fold f1 f2 f3 f4 f5 f6 f7 y) 
fold f1 f2 f3 f4 f5 f6 f7 (x :/ y) = f4 (fold f1 f2 f3 f4 f5 f6 f7 x) (fold f1 f2 f3 f4 f5 f6 f7 y) 
fold f1 f2 f3 f4 f5 f6 f7 (x :* y) = f5 (fold f1 f2 f3 f4 f5 f6 f7 x) (fold f1 f2 f3 f4 f5 f6 f7 y) 
fold f1 f2 f3 f4 f5 f6 f7 (Let x y z) = f6 ()
-- Tambahkan konstruksi Let pada struktur data Ekspresi menjadi:
data ExprV2 = C Float | ExprV2 :+ ExprV2 | ExprV2 :- ExprV2
    | ExprV2 :* ExprV2 | ExprV2 :/ ExprV2
    | V String
    | Let String ExprV2 ExprV2
    deriving Show

-- Sesuaikan fungsi evaluasi versi sebelumnya (versi definisi rekursif).
evaluate ls (C x) = x
evaluate ls (x :+ y) = (evaluate ls x) + (evaluate ls y)
evaluate ls (x :- y) = (evaluate ls x) - (evaluate ls y)
evaluate ls (x :* y) = (evaluate ls x) * (evaluate ls y)
evaluate ls (x :/ y) = (evaluate ls x) / (evaluate ls y)
evaluate ls (V y) = evaluate ls (lookup ls)
    where   lookup (x:xs)
                | (fst x) == y = snd x  
                | otherwise = lookup xs
            lookup [] = error "binding error!"
evaluate ls (Let x y z) = evaluate ((x, y):ls) z
-- bisa di test x = Let "xx" (C 3) (V "xx" :+ C 14)

-- Sesuaikan fungsi evaluasi versi menggunakan fold.
foldExprV2 fx ft fk fm fb fv ls (C x) = fx x
foldExprV2 fx ft fk fm fb fv ls (x :+ y) = ft (foldExprV2 fx ft fk fm fb fv ls x) (foldExprV2 fx ft fk fm fb fv ls y)
foldExprV2 fx ft fk fm fb fv ls (x :- y) = fk (foldExprV2 fx ft fk fm fb fv ls x) (foldExprV2 fx ft fk fm fb fv ls y)
foldExprV2 fx ft fk fm fb fv ls (x :* y) = fm (foldExprV2 fx ft fk fm fb fv ls x) (foldExprV2 fx ft fk fm fb fv ls y)
foldExprV2 fx ft fk fm fb fv ls (x :/ y) = fb (foldExprV2 fx ft fk fm fb fv ls x) (foldExprV2 fx ft fk fm fb fv ls y)
foldExprV2 fx ft fk fm fb fv ls (V y) = foldExprV2 fx ft fk fm fb fv ls (fv (lookup ls))
    where   lookup (x:xs)
                | (fst x) == y = snd x  
                | otherwise = lookup xs
            lookup [] = error "binding error!"
foldExprV2 fx ft fk fm fb fv ls (Let x y z) = foldExprV2 fx ft fk fm fb fv ((x, y):ls) z

evaluateV2 = foldExprV2 fx ft fk fm fb fv
    where   fx = \x->x
            ft = (+)
            fk = (-)
            fm = (*)
            fb = (/)
            fv = \x->x

--Bandingkan proses modifikasi yang perlu dilakukan
-- menurut saya, saya jadi kuli

--Gunakan fold untuk mendefinisikan fungsi terhadap struktur data tersebut seperti misalnya
--menghitung jumlah konstanta yang digunakan, jumlah operator, jumlah variable, melihat
--apakah ada pembagian by zero atau tidak

--jumlah konstanta yang digunakan

    


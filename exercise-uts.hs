-- Rancanglah, higher order function yang bekerja pada struktur data ekspresi dan memiliki
-- semantik seperti: Fungsi map pada list, Fungsi fold pada tree

-- To simplify stuff, addition and substraction only
data Expr = C Float 
    | Add Expr Expr
    | Sub Expr Expr
    deriving Show

evaluate::Expr->Float
evaluate (C x) = x
evaluate (Add x y) = (evaluate x) + (evaluate y)
evaluate (Sub x y) = (evaluate x) - (evaluate y)

--mapConst, untuk merubah setiap konstanta ekspresi
mapConst::(Float->Float)->(Expr)->(Expr)
mapConst f (C x) = C (f x)
mapConst f (Add x y) = Add (mapConst f x) (mapConst f y)
mapConst f (Sub x y) = Sub (mapConst f x) (mapConst f y)

--foldExpr, untuk melakukan folding
foldExpr fx fa fs (C x) = fx x
foldExpr fx fa fs (Add x y) = fa (foldExpr fx fa fs x) (foldExpr fx fa fs y)
foldExpr fx fa fs (Sub x y) = fs (foldExpr fx fa fs x) (foldExpr fx fa fs y)

-- Pada buku dan contoh sebelumnya, fungsi evaluasi didefinisikan secara rekursif. Gunakan
-- higher order function yang baru dibuat untuk mendefinisikan fungsi evaluasi tersebut.
-- Pastikan memiliki makna semantik yang sama dengan definisi sebelumnya. 
evaluateUsingFold = foldExpr fx fa fs
    where fx = \x->x
          fa = (+)
          fs = (-)


-- selanjutnya di exprv2.hs


